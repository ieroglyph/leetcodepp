#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"

#include "solution.hpp"
using Problem0011::Solution;

TEST_CASE( "container-with-most-water", "[Problem0011]" ) {
    Solution solution;
    REQUIRE( solution.maxArea({1,1}) == 1 );
    REQUIRE( solution.maxArea({4,3,2,1,4}) == 16 );
    REQUIRE( solution.maxArea({1,2,1}) == 2 );
}
