#include <vector>

using namespace std;

/*
11. Container With Most Water
Medium

Given n non-negative integers a1, a2, ..., an , where each represents a point at coordinate (i, ai). n vertical lines are drawn such that the two endpoints of the line i is at (i, ai) and (i, 0). Find two lines, which, together with the x-axis forms a container, such that the container contains the most water.

Notice that you may not slant the container.



Example 1:

Input: height = [1,8,6,2,5,4,8,3,7]
Output: 49
Explanation: The above vertical lines are represented by array [1,8,6,2,5,4,8,3,7]. In this case, the max area of water (blue section) the container can contain is 49.

Example 2:

Input: height = [1,1]
Output: 1

Example 3:

Input: height = [4,3,2,1,4]
Output: 16

Example 4:

Input: height = [1,2,1]
Output: 2



Constraints:

    n == height.length
    2 <= n <= 10^5
    0 <= height[i] <= 10^4

*/
namespace Problem0011
{
class Solution {
public:

using vi = vector<int>;
    int maxArea(const vector<int>& height) {
        auto i1 = height.begin();
        auto i2 = std::prev(height.end());
        auto dist = distance(i1, i2);
        int maxArea = 0;
        while(i1 != i2)
        {
            const auto h1 = *i1;
            const auto h2 = *i2;
            auto area = (dist * min(h1, h2));
            if (maxArea < area)
                maxArea = area;
            if (h1 > h2)
                i2 = std::prev(i2);
            else
                i1 = std::next(i1);
            dist--;
        }
        return maxArea;
    }
};
}
