#include <limits>
#include <vector>
#include <string>

/*
43. Multiply Strings
Medium

Given two non-negative integers num1 and num2 represented as strings, return the product of num1 and num2, also represented as a string.

Note: You must not use any built-in BigInteger library or convert the inputs to integer directly.

Example 1:

Input: num1 = "2", num2 = "3"
Output: "6"

Example 2:

Input: num1 = "123", num2 = "456"
Output: "56088"

Constraints:

    1 <= num1.length, num2.length <= 200
    num1 and num2 consist of digits only.
    Both num1 and num2 do not contain any leading zero, except the number 0 itself.

*/

using namespace std;

namespace Problem0043
{

class Solution {
public:
    string multiply(string num1, string num2) {
        if (num1.at(0) == '0' || num2.at(0) == '0')
            return "0";
        const auto l1 = num1.size(); // length1
        const auto l2 = num2.size(); // length2
        string ans(l1 + l2, '0');
        char carry = 0;
        for (size_t i1 = l1; i1; i1--) {
            const auto d1 = num1.at(i1 - 1) - '0'; // digit1
            for (size_t i2 = l2; i2; i2--) {
                const auto d2 = num2.at(i2 - 1) - '0'; // digit2
                const auto m = d1 * d2;
                ans[i1 + i2 - 1] += m%10;
                carry = (ans[i1 + i2 - 1] - '0') / 10;
                ans[i1 + i2 - 1] = '0' + (ans[i1 + i2 - 1]-'0')%10;
                ans[i1 + i2 - 2] += m / 10 + carry;
            }
        }
        if (ans.at(0) == '0')
            ans.erase(ans.cbegin());
        return ans;
    }
};

}
