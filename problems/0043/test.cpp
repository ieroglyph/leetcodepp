#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"

#include "solution.hpp"
#include <utility>

TEST_CASE( "multiply", "[Problem0043]" ) {
    Problem0043::Solution s;

    vector<tuple<string, string, string>> testCases =
    {
        {
            "2", "0", "0"
        },
        {
            "0", "3", "0"
        },
        {
            "123", "456", "56088"
        },
        {
            "456", "123", "56088"
        },
        {
            "321", "654", "209934"
        },
        {
            "2", "3", "6"
        },
        {
            "11", "11", "121"
        },
        {
            "999", "999", "998001"
        }
    };

    for (auto [num1, num2, result] : testCases)
    {
        REQUIRE(s.multiply(num1, num2) == result);
    }
}
