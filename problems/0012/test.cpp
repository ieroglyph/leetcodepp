#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"

#include "solution.hpp"

TEST_CASE( "intToRoman", "[Problem0012]" ) {
    Problem0012::Solution solution;
    REQUIRE( solution.intToRoman(1) == "I" );
    REQUIRE( solution.intToRoman(10) == "X" );
    REQUIRE( solution.intToRoman(1989) == "MCMLXXXIX" );
    REQUIRE( solution.intToRoman(100) == "C" );
    REQUIRE( solution.intToRoman(1000) == "M" );
    REQUIRE( solution.intToRoman(3999) == "MMMCMXCIX" );
    REQUIRE( solution.intToRoman(58) == "LVIII" );
}
