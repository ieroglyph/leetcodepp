#include <string>
#include <map>

using namespace std;

namespace Problem0012
{
class Solution {
    const static map<int,string> dict;
public:
    string intToRoman(int num) {
        string result = "";
        result.reserve(16);
        const int Is = num % 10;
        num -= Is;
        const int Xs = num % 100;
        num -= Xs;
        const int Cs = num % 1000;
        num -= Cs;
        int Ms = num / 1000;
        while (Ms--)
            result.append("M");
        result.append(dict.at(Cs));
        result.append(dict.at(Xs));
        result.append(dict.at(Is));
        return result;
    }
};

const map<int,string> Solution::dict = {
    {1,"I"},    {10,"X"},    {100,"C"},     {0,""},
    {2,"II"},   {20,"XX"},   {200,"CC"},
    {3,"III"},  {30,"XXX"},  {300,"CCC"},
    {4,"IV"},   {40,"XL"},   {400,"CD"},
    {5,"V"},    {50,"L"},    {500,"D"},
    {6,"VI"},   {60,"LX"},   {600,"DC"},
    {7,"VII"},  {70,"LXX"},  {700,"DCC"},
    {8,"VIII"}, {80,"LXXX"}, {800,"DCCC"},
    {9,"IX"},   {90,"XC"},   {900,"CM"},
};

}
