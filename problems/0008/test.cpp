#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"

#include "solution.hpp"

TEST_CASE( "myAtoi", "[Problem0008]" ) {

    Problem0008::Solution s;

    REQUIRE(s.myAtoi("42") == 42);
    REQUIRE(s.myAtoi("   -42") == -42);
    REQUIRE(s.myAtoi(" +0042") == 42);
    REQUIRE(s.myAtoi("4193 with words") == 4193);
    REQUIRE(s.myAtoi("words and 987") == 0);
    REQUIRE(s.myAtoi("-9128347233") == -2147483648);
    REQUIRE(s.myAtoi("9128347233") == 2147483647);
}
