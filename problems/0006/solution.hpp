using namespace std;

/*
6. ZigZag Conversion
Medium

The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)

P   A   H   N
A P L S I I G
Y   I   R

And then read line by line: "PAHNAPLSIIGYIR"

Write the code that will take a string and make this conversion given a number of rows:

string convert(string s, int numRows);

Example 1:

Input: s = "PAYPALISHIRING", numRows = 3
Output: "PAHNAPLSIIGYIR"

Example 2:

Input: s = "PAYPALISHIRING", numRows = 4
Output: "PINALSIGYAHRPI"
Explanation:
0     6    12         0   4   8   12     0 2 4 6 8 1012   0       8
P     I    N          P   A   H   N      P Y S I H I G    P       H
A   L S  I G          A P L S I I G      A P L S R N      A     S R
Y A   H R             Y   I   R                           Y   I   I
P     I                                                   P L     N
                                                          A       G

1   5 7
P     I    N          P   A   H   N      P Y S I H R N    P       H    a
A   L S  I G          A P L S I I G      A P L S I I G    A     S R    b d
Y A   H R             Y   I   R                           Y   I   I    c
P     I                                                   P L     N
                                                          A       G



* */

namespace Problem0006
{

class Solution {
public:
    string convert(string s, int numRows) {
        if (numRows == 1) return s;
        string r;
        size_t step = 2 * numRows - 2;
        for (size_t i = 0; i < s.size(); i += step)
            r += s[i];
        for (int row = 1; row < numRows-1; row += 1)
        {
            for (size_t i = 0; i < s.size() + row; i += step)
            {
                if (i >= step) r += s[i-row];
                if (i+row < s.size()) r += s[i+row];
            }
        }
        for (size_t i = numRows-1; i < s.size(); i += step)
            r += s[i];
        return r;
    }
};

}

