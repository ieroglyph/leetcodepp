#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"

#include "solution.hpp"

TEST_CASE( "convert", "[Problem0006]" ) {
    Problem0006::Solution s;
    REQUIRE(s.convert("PAYPALISHIRING", 1) == string("PAYPALISHIRING"));
    REQUIRE(s.convert("PAYPALISHIRING", 2) == string("PYAIHRNAPLSIIG"));
    REQUIRE(s.convert("PAYPALISHIRING", 3) == string("PAHNAPLSIIGYIR"));
    REQUIRE(s.convert("PAYPALISHIRING", 4) == string("PINALSIGYAHRPI"));
    REQUIRE(s.convert("PAYPALISHIRING", 14) == string("PAYPALISHIRING"));
    REQUIRE(s.convert("PAYPALISHIRING", 98) == string("PAYPALISHIRING"));
    REQUIRE(s.convert("ABCD", 3) == string("ABDC"));
}
