#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"

#include "solution.hpp"

TEST_CASE( "longestPalindrome", "[Problem0005]" ) {
    Problem0005::Solution s;
    REQUIRE(s.longestPalindrome("babad") == string("bab"));
    REQUIRE(s.longestPalindrome("cdde") == string("dd"));
    REQUIRE(s.longestPalindrome("bacab") == string("bacab"));
    REQUIRE(s.longestPalindrome("q") == string("q"));
    REQUIRE(s.longestPalindrome("eebacababba") == string("bacab"));
}
