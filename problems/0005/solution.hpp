#include <string>

using namespace std;

namespace Problem0005
{

class Solution {
public:
    inline bool check_for_palindrome(string::iterator b, string::iterator e) const
    {
        for (; b < e; b++, e--)
            if (*b != *e) return false;
        return true;
    }
    string longestPalindrome(string s) {
        string::iterator rb = s.begin();
        string::iterator re = rb;
        string::iterator b = s.begin();
        string::iterator e = s.end()-1;
        while(b < e && s.end() - b > re - rb)
        {
            string::iterator e = s.end()-1;
            while (e - b > re - rb)
            {
                if (check_for_palindrome(b, e))
                {
                    rb = b;
                    re = e;
                }
                e--;
            }
            b++;
        }
        return string(rb, re+1);
    }
};

}
