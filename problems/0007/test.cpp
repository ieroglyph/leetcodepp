#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"

#include "solution.hpp"

TEST_CASE( "reverse", "[Problem0007]" ) {
    Problem0007::Solution s;
    REQUIRE(s.reverse(123) == 321);
    REQUIRE(s.reverse(-123) == -321);
    REQUIRE(s.reverse(120) == 21);
    REQUIRE(s.reverse(-120) == -21);
    REQUIRE(s.reverse(233) == 332);
    REQUIRE(s.reverse(-233) == -332);
    REQUIRE(s.reverse(0) == 0);
    REQUIRE(s.reverse(1) == 1);
    REQUIRE(s.reverse(-1) == -1);
    REQUIRE(s.reverse(12) == 21);
    REQUIRE(s.reverse(-12) == -21);
    REQUIRE(s.reverse(900000) == 9);
    REQUIRE(s.reverse(-900000) == -9);
    REQUIRE(s.reverse(-1534236469) == 0);
    REQUIRE(s.reverse(1534236469) == 0);
}
