#include <cmath>

using namespace std;

/*
7. Reverse Integer
Easy

Given a 32-bit signed integer, reverse digits of an integer.

Example 1:

Input: 123
Output: 321

Example 2:

Input: -123
Output: -321

Example 3:

Input: 120
Output: 21

Note:
Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−231,  231 − 1]. For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.

*/

namespace Problem0007
{

class Solution {
public:
    int reverse(int x) {
        int result = 0;
        do
        {
            if (result >= ((1 << 31) - 1)/10 || result <= -1 * (1 << 31)/10) return 0;
            result *= 10;
            result += x % 10;
            x /= 10;
        }
        while(x != 0);
        if (result != 0 && x / result < 0) return 0;
        return result;
    }
};

}
