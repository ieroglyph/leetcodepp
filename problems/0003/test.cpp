#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"

#include "solution.hpp"

TEST_CASE( "findMedianSortedArrays", "[Problem0003]" ) {
    Problem0003::Solution solution;
    REQUIRE( solution.lengthOfLongestSubstring("3aaa4bbbb2cc6dddddd") == 3 );
}
