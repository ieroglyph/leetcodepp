#include <string>

using namespace std;

namespace Problem0003
{
class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        bool seen_chars[255];
        for (int i = 0; i < 255; i++) seen_chars[i] = false;
        int sbegin = 0;
        int send = 0;
        int maxlength = 0;
        for (unsigned int i = 0; i < s.length(); i++)
        {
            char c = s[send];
            unsigned char uc = static_cast<unsigned char>(c);
            if (!seen_chars[uc])
            {
                seen_chars[uc] = true;
            }
            else
            {
                maxlength = max(maxlength, send - sbegin);
                while(s[sbegin] != c)
                {
                    seen_chars[static_cast<unsigned char>(s[sbegin])] = false;
                    sbegin++;
                }
                sbegin++;
            }
            send++;
        }
        return max(maxlength, send - sbegin);
    }
};
}
