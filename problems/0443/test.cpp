#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"

#include "solution.hpp"
#include <utility>

TEST_CASE( "minPathSum", "[Problem0064]" ) {
    Problem0443::Solution s;

    vector<std::pair<vector<char>,vector<char>>> testCases =
    {
        {{'r','r','d','d'},{'r','2','d','2'}},
        {{'c','c','c','p','o'},{'c','3','p','o'}},
        {{'a'},{'a'}},
        {{'a','a','a','a','a','a','a','a','a','a','a','a','a'},{'a','1','3'}}
    };

    for (auto testCase : testCases)
    {
        s.compress(testCase.first);
        REQUIRE(testCase.first == testCase.second);
    }
}
