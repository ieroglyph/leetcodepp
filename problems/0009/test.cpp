#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"

#include "solution.hpp"

TEST_CASE( "isPalindrome", "[Problem0009]" ) {
    Problem0009::Solution s;

    REQUIRE(s.isPalindrome(42) == false);
    REQUIRE(s.isPalindrome(420) == false);
    REQUIRE(s.isPalindrome(220) == false);
    REQUIRE(s.isPalindrome(-121) == false);
    REQUIRE(s.isPalindrome(121) == true);
    REQUIRE(s.isPalindrome(1) == true);
    REQUIRE(s.isPalindrome(10) == false);
    REQUIRE(s.isPalindrome(0) == true);
}
