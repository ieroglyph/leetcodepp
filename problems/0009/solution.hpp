#include <limits>

/*
9. Palindrome Number
Easy

Determine whether an integer is a palindrome. An integer is a palindrome when it reads the same backward as forward.

Example 1:

Input: 121
Output: true

Example 2:

Input: -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.

Example 3:

Input: 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.

Follow up:

Coud you solve it without converting the integer to a string?

*/

using namespace std;

namespace Problem0009
{

class Solution {
public:
    bool isPalindrome(int x) {
        if (x < 0) return false;
        int iks = x;
        int ski = 0;
        while (x != 0)
        {
            if (ski > std::numeric_limits<int>::max() / 10 ||
                (ski * 10) > (std::numeric_limits<int>::max() - x % 10))
                return false;
            ski *= 10;
            ski += x % 10;
            x /= 10;
        }
        return iks == ski;
    }
};

}
