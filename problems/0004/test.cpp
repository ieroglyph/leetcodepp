#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"
using namespace Catch::literals;

#include "solution.hpp"

TEST_CASE( "findMedianSorterArray", "[Problem0004]" ) {
    Problem0004::Solution solution;

    REQUIRE(solution.findMedianSortedArrays({1,3}, {2}) == 2._a);
    REQUIRE(solution.findMedianSortedArrays({1,2},{3,4}) == 2.5_a);
    REQUIRE(solution.findMedianSortedArrays({1,2,3},{5,6,7}) == 4._a);
    REQUIRE(solution.findMedianSortedArrays({1},{2,3,4,5}) == 3._a);
    REQUIRE(solution.findMedianSortedArrays({1},{2}) == 1.5_a);
    REQUIRE(solution.findMedianSortedArrays({},{2}) == 2._a);
    REQUIRE(solution.findMedianSortedArrays({1,2,3},{}) == 2._a);
    REQUIRE(solution.findMedianSortedArrays({-3,-2,-1},{1,2,3}) == 0_a);
}
