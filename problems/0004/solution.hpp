#include <vector>

using namespace std;

namespace Problem0004
{

class Solution {
public:
    double findMedianSortedArrays(vector<int> nums1, vector<int> nums2) {
        int s1 = nums1.size();
        int s2 = nums2.size();

        bool odd = (s1 + s2) % 2 != 0;
        int result_size = (s1 + s2) / 2 + 1;
        int i1 = 0;
        int i2 = 0;
        int m1 = 0;
        int m2 = 0;
        for (int i = 0; i < result_size; i++)
        {
            if (i1 < s1 && i2 < s2)
            {
                if (nums1[i1] < nums2[i2])
                {
                    m1 = m2;
                    m2 = nums1[i1];
                    i1++;
                }
                else
                {
                    m1 = m2;
                    m2 = nums2[i2];
                    i2++;
                }
            }
            else
                if (i1 < s1)
                {
                    m1 = m2;
                    m2 = nums1[i1];
                    i1++;
                }
                else {
                    m1 = m2;
                    m2 = nums2[i2];
                    i2++;
                }
        }
        return static_cast<double>((odd ? m2 : m1) + m2) / 2.;
    }
};

}
