#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"

#include "solution.hpp"
#include <utility>

TEST_CASE( "minPathSum", "[Problem0064]" ) {
    Problem0064::Solution s;

    vector<pair<vector<vector<int>>, int>> testCases =
    {
        {
            {{1, 3, 1},
             {1, 5, 1},
             {4, 2, 1}},
            7
        },
        {
            {{1}},
            1
        },
        {
            {{0}},
            0
        },
        {
            {{1, 1, 1},
             {1, 1, 1},
             {1, 1, 1}},
            5
        },
        {
            {{0, 0},
             {0, 0}},
            0
        },
        {
            {{0, 1, 2},
             {1, 0, 1},
             {2, 1, 0}},
            2
        },
    };

    for (auto testCase : testCases)
    {
        REQUIRE(s.minPathSum(testCase.first) == testCase.second);
    }
}
